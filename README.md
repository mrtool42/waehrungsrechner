# Wähungsrechner

Meine ersten Schritte mit C#.
Ein kleiner Währungsrechner, mit dem man einen Euro-Betrag in verschiedene Währungen umrechnen kann.
Wechselkurse sind Konstanten und stimmen nicht mit dem aktuellen Kurs überein!

#### Folgende Währungen können berechnet werden
- US - Dollar
- Vietnamesische Dong
- Venezuelanische Bolivar
- Britische Pfund
- Schweizer Franken
- Indische Rupia