﻿
//************* Währungsrechner *************//
//**          Mario Rammelmüller           **//
//**                2ABIF                  **//
//**             042@aikq.de               **//
//*******************************************//


using System;

using System.Globalization;

namespace waehrungsrechner
{
    class Program
    {
        private const string invalid = "Ungültige Eingabe!";

        static void Main(string[] args)
        {
            const decimal usd = 1.23m;
            const decimal vnd = 27452.74m;
            const decimal vef = 284299.47m;
            const decimal gbp = 1.1979m;
            const decimal chf = 1.1054m;
            const decimal inr = 85.8345m;
            int choice;
            decimal betrag, wert, result;


            CultureInfo deDE = CultureInfo.CreateSpecificCulture("de-DE");


            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Willkommen beim Wähungsrechner!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine("Wolltest du schon immer wissen, welches Vermögen du in Vietnam oder gar in Venezuela besitzen würdest?");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Dann bist du hier genau richtig!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine();
            Console.WriteLine("                               Drücke die ENTER Taste um fortzufahren.");
            Console.ReadKey();

        start:
            {
                Console.Clear();
                Console.WriteLine("          Na gut!");
                Console.WriteLine("Sag mir wie viele Euro du hast: ");


            failstart:
                {
                    string usrinput1 = Console.ReadLine();

                    if (decimal.TryParse(usrinput1, out betrag))
                    {
                        Console.Clear();
                        Console.WriteLine("Alles klar!");                        
                        Console.WriteLine("In welche Währung möchtest du umrechnen?");
                        Console.WriteLine();
                        Console.WriteLine("(1) US-Dollar");
                        Console.WriteLine("(2) Vietnamesische Dong");
                        Console.WriteLine("(3) Venezuelanische Bolivar");
                        Console.WriteLine("(4) Britische Pfund");
                        Console.WriteLine("(5) Schweizer Franken");
                        Console.WriteLine("(6) Indische Rupien");
                    }
                    else
                    {
                        Console.WriteLine(invalid);
                        goto failstart;

                    }
                }
            }


        choose:
            {

                string usrinput2 = Console.ReadLine();

                if (int.TryParse(usrinput2, out choice))
                {

                    if (choice == 1)
                    {

                        Console.Clear();
                        wert = (betrag * usd);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Amerikanische Dollar.")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }

                    else if (choice == 2)
                    {

                        Console.Clear();
                        wert = (betrag * vnd);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Vietnamesische Dong.  Nicht schlecht!")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                    else if (choice == 3)
                    {

                        Console.Clear();
                        wert = (betrag * vef);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Venezuelanische Bolivar.  Wahnsinn!")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                    else if (choice == 4)
                    {

                        Console.Clear();
                        wert = (betrag * gbp);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Britische Pfund.")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                    else if (choice == 5)
                    {

                        Console.Clear();
                        wert = (betrag * chf);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Schweizer Franken.")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                    else if (choice == 6)
                    {

                        Console.Clear();
                        wert = (betrag * inr);
                        result = Math.Round(wert, 2);
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        Console.WriteLine((betrag.ToString("0,0.00", deDE) + " Euro sind umgerechnet " + (result.ToString("0,0.00", deDE) + " Indische Rupien.")));
                        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    }
                    else
                    {

                        Console.WriteLine(invalid);
                        goto choose;

                    }
                }
                else
                {
                    Console.WriteLine(invalid);
                    goto choose;
                }

            }
        ask:
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Nochmal? (ja/nein)");
            }
                string input = Convert.ToString(Console.ReadLine());
            
            if (input == "ja")
            {
                goto start;
            }
            else if (input == "nein")
            {
                Console.Clear();
                Console.WriteLine("Auf Wiedersehen!");
            }
            else
            {
                Console.Clear();

                Console.WriteLine(invalid);
                goto ask;
            }

        }
    }
}
